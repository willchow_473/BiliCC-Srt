# BiliCC-Srt

#### 介绍
用于下载BiliBili CC字幕的工具。

用QT5制作，另有Python版。

#### 小说明

- BV号使用：不需要也不可输入开头的BV两字，如BV1w4411P7xn，应输入1w4411P7xn。
- 查看BV号：URL www.bilibili.com/video/BV1w4411P7xn?from=search 中，BV号为BV二字到问号前，即 1w4411P7xn。
- 
- 字幕路径：下载字幕后，字幕文件位于EXE根目录（和EXE相同位置） 
- MAC用户：可以尝试下载Python版，据反馈可以正常使用。
- 附个测试例子BV号：13t411U7Vm

#### 额外问题

- 乱码问题：似乎是暴风影音对UTF-8支持不太好，若有需要，可以尝试使用GB2312编码。
- 无法启动：请安装"Microsoft Visual C++ 2015-2019 Redistributable"(百度“微软运行库合集”下载安装亦可)

#### 蓝奏云
https://kgd.lanzoux.com/b00naiahe 密码:ahxs